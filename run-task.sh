if [[ "$(docker images -q worker 2> /dev/null)" == "" ]]; then
	echo "Docker is downloading and building image. Please wait."
	docker build -t worker worker/ 1> /dev/null
fi

if [[ $1 = "" ]]; then
	docker run -e "PGPASSWORD=mysecretpassword" --name worker --rm --link postgres:postgres worker ./check-hours.py
elif [[ $1 = "test" ]] && [[ $2 = "a" ]]; then
	docker run -e "PGPASSWORD=mysecretpassword" --name worker --rm --link postgres:postgres worker ./test-api.sh
elif [[ $1 = "test" ]] && [[ $2 = "b" ]]; then
	docker run -e "PGPASSWORD=mysecretpassword" --name worker --rm --link postgres:postgres worker ./test-content-table.sh
elif [[ $1 = "test" ]]; then
	docker run -e "PGPASSWORD=mysecretpassword" --name worker --rm --link postgres:postgres worker ./test-api.sh
	docker run -e "PGPASSWORD=mysecretpassword" --name worker --rm --link postgres:postgres worker ./test-content-table.sh
fi
