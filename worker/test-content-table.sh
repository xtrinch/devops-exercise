#!/usr/bin/env bash

start='2016-10-01'
end='2016-10-13'
output=$(psql -h postgres -U postgres postgres -c "select (case when count(*) = -DATE_PART('day', ('${start}'::timestamp - '${end}'::timestamp))+1 then 'true' else 'false' end) as all_full from (select date from content_table where date between '${start}' and '${end}' group by date having (sum(hour) = 276 and min(hour) = 0 and max(hour) = 23 or sum(range) = 24)) as dates;")
echo $output
