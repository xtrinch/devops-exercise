#!/usr/bin/env python
import time, psycopg2, math, requests
from datetime import date, datetime, time, timedelta

# connect to database with hostname postgres
conn = psycopg2.connect("host=postgres dbname=postgres user=postgres")

# open a cursor to perform database operations
cur = conn.cursor()

# execute a sample statement
cur.execute("select date, hour from status_table order by date, hour;")
statuses = cur.fetchall()

# naively presuming we've got at least one entry
prev_dt = datetime.combine(statuses[0][0], time(statuses[0][1], 0)) - timedelta(hours=1)
success = True

for row in statuses:

	# combine date and hour to datetime object
	hour = time(row[1], 0)
	curr_dt = datetime.combine(row[0], hour)

	# check if there are gaps before the date we have just taken out of statuses array
	if curr_dt - timedelta(hours=1) != prev_dt:
		
		# calculate timedelta
		delta = curr_dt - prev_dt
		missing_hours = (delta.seconds // 3600) - 1 + (delta.days * 24)
		missing_date = prev_dt + timedelta(hours=1)

		# loop, until the missing date increases to date we have taken out of statuses array
		while (missing_date != curr_dt):

			# if missing date increased to another day, and we have atleast 24 more hours missing, call day api
			if missing_hours >= 24 and missing_date.day != (missing_date - timedelta(hours=1)).day:
				url = 'http://mockbin.org/bin/d21a0e91-05aa-491b-a4a0-d795aeadb24d'
				try:
					r = requests.get(url).json()
				except:
					print("Error communicating with mockbin. Restore database to its original state and retry.")
					success = False
				cur.execute("insert into content_table values('%s', %d, '%s', %d, '%s')" % (missing_date.strftime('%Y-%m-%d'), int(missing_date.strftime('%H')), r['type'], r['range'], r['text']))
				missing_date = missing_date + timedelta(days=1)	
				missing_hours = missing_hours - 24
			else:
				url = 'http://mockbin.org/bin/1f070f39-3781-4213-8fe1-71e072fb9128'
				try:
					r = requests.get(url).json()
				except:
					print("Error communicating with mockbin. Restore database to its original state and retry.")
					success = False
				cur.execute("insert into content_table values('%s', %d, '%s', %d, '%s')" % (missing_date.strftime('%Y-%m-%d'), int(missing_date.strftime('%H')), r['type'], r['range'], r['text']))
				missing_hours = missing_hours - 1
				missing_date = missing_date + timedelta(hours=1)



	prev_dt = curr_dt

# close communcation with the database
conn.commit()
cur.close()
conn.close()

if success:
	print("Success.")
