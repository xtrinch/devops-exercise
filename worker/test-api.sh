#!/usr/bin/env bash

export PYTHONIOENCODING=utf8

for i in {1..10}
do
	rand=$(shuf -i 1-4 -n 1)
	case $rand in

	1) url="http://mockbin.org/bin/f017a29e-8ef5-4929-9219-542c5016af4e"
		;;
	2) url="http://mockbin.org/bin/3379d830-7fd9-4c43-8fce-eb0b91145697"
		;;
	3) url="http://mockbin.org/bin/1f070f39-3781-4213-8fe1-71e072fb9128"
		;;
	4) url="http://mockbin.org/bin/d21a0e91-05aa-491b-a4a0-d795aeadb24d"
		;;
	esac

	output=$(curl -s -w " %{http_code}" $url)
	status_code=$(echo $output | tail -1 | awk '{print $NF}')
	json=$(echo $output | sed "s/$status_code//g")

	echo -ne "$status_code: "
	if [[ $status_code == 200 ]]; then
		echo $json | python -c "import sys, json; txt = json.load(sys.stdin); print txt['text'] + ' (' + txt['type'] + ')'"
	elif [[ $status_code == 500 ]] || [[ $status_code == 400 ]]; then
		echo $json | python -c "import sys, json; print json.load(sys.stdin)['error']"	
	fi

done
