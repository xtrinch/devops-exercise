# devops-exercise

## Requirements

	- Bash, Docker

## Usage

	- Seed database with ./seed-database.sh
	- Run main task with ./run-task.sh (note: if run multiple times, it will create duplicates in database, and the final test will fail) 
	- Run API tests with /run-task.sh test a (note: single endpoint testing with multiple URL's is simulated so that one is selected at random each time)
	- Run database test with ./run-task.sh test b
	
## Additional info

	- Not exactly an "application package", but the worker docker image could be put on a docker registry
	